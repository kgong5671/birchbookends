from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views import generic


from .forms import NameForm
from .models import Question, Choice, Books, Works

# Create your views here

def HomeViewFunction(request):
    return render(request, 'bookends/home.html')

def PaymentsViewFunction(request):
    return render(request, 'bookends/payments.html')

def LoginViewFunction(request):
    return render(request, 'bookends/login.html')

def PaymentsTestViewFunction(request):
    template_name = 'bookends/books.html'
    context_object_name = 'all_books'
    
    def get_queryset(self):
        """Return the last five published questions."""
        return Books.objects.order_by('title')
    return render(request, 'bookends/payments_form.html')

def BooksViewFunction(request):
    template_name = 'bookends/books.html'
    books_list = Books.objects.all()
    works_list = Works.objects.all()
    return render(request, 'bookends/books.html', {'books': books_list, 'works': works_list})

def WorksViewFunction(request):
    template_name = 'bookends/works.html'
    books_list = Books.objects.all()
    works_list = Works.objects.all()
    return render(request, 'bookends/works.html', {'books': books_list, 'works': works_list})

def WorksDB(request):
    works_list = Works.objects.order_by('isbn')[:10]
    return render(request, 'bookends/worksDB.html', {'works': works_list})

def BooksDB(request):
    books_list = Books.objects.order_by('isbn')[:10]
    return render(request, 'bookends/booksDB.html', {'books': books_list})

class HomeView(generic.ListView):
    template_name='bookends/home.html'

class IndexView(generic.ListView):
    template_name = 'bookends/index.html'
    context_object_name = 'latest_question_list'
    
    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.order_by('-pub_date')[:5]

class BooksView(generic.ListView):
    template_name = 'bookends/books.html'
    context_object_name = 'all_books'
    
    def get_queryset(self):
        """Return the last five published questions."""
        return Books.objects.order_by('title')


class DetailView(generic.DetailView):
    model = Question
    template_name = 'bookends/detail.html'


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'bookends/results.html'


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'bookends/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('bookends:results', args=(question.id,)))
def payments(request):
    return HttpResponseRedirect(reverse('bookends:results'))
def get_name(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NameForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect('/thanks/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = NameForm()

    return render(request, 'name.html', {'form': form})