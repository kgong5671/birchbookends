from django.apps import AppConfig


class BookendsConfig(AppConfig):
    name = 'bookends'
