from django.db import models
import datetime
from django.utils import timezone

# Create your models here.

class Question(models.Model):
	question_text = models.CharField(max_length=200)
	pub_date = models.DateTimeField('date published')
	def __str__(self):
		return self.question_text
	def was_published_recently(self):
		return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

class Choice(models.Model):
	question = models.ForeignKey(Question, on_delete=models.CASCADE)
	choice_text = models.CharField(max_length=200)
	votes = models.IntegerField(default=0)
	def __str__(self):
		return self.choice_text

class Addresses(models.Model):
    shippingaddress = models.CharField(db_column='ShippingAddress', primary_key=True, max_length=50)  # Field name made lowercase.
    stateprovince = models.CharField(db_column='StateProvince', max_length=40, blank=True, null=True)  # Field name made lowercase.
    country = models.CharField(db_column='Country', max_length=60, blank=True, null=True)  # Field name made lowercase.
    city = models.CharField(db_column='City', max_length=30, blank=True, null=True)  # Field name made lowercase.
    surveyid = models.IntegerField(db_column='SurveyID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'addresses'


class Books(models.Model):
    bookid = models.IntegerField(db_column='BookID', primary_key=True)  # Field name made lowercase.
    available = models.IntegerField(db_column='Available', blank=True, null=True)  # Field name made lowercase.
    location = models.CharField(db_column='Location', max_length=40, blank=True, null=True)  # Field name made lowercase.
    isbn = models.CharField(db_column='ISBN', max_length=13, blank=True, null=True)  # Field name made lowercase.
    requestid = models.IntegerField(db_column='RequestID', blank=True, null=True)  # Field name made lowercase.
    donationid = models.IntegerField(db_column='DonationID', blank=True, null=True)  # Field name made lowercase.
	
    class Meta:
        managed = False
        db_table = 'books'



class Children(models.Model):
    childid = models.IntegerField(db_column='ChildID', primary_key=True)  # Field name made lowercase.
    readinglevel = models.CharField(db_column='ReadingLevel', max_length=40, blank=True, null=True)  # Field name made lowercase.
    interests = models.CharField(db_column='Interests', max_length=50, blank=True, null=True)  # Field name made lowercase.
    clientid = models.IntegerField(db_column='ClientID', blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=50, blank=True, null=True)  # Field name made lowercase.
    age = models.IntegerField(db_column='Age', blank=True, null=True)  # Field name made lowercase.
    surveyid = models.IntegerField(db_column='SurveyID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'children'


class Clients(models.Model):
    clientid = models.IntegerField(db_column='ClientID', primary_key=True)  # Field name made lowercase.
    requestid = models.ForeignKey('Requests', models.DO_NOTHING, db_column='RequestID', blank=True, null=True)  # Field name made lowercase.
    username = models.CharField(db_column='Username', max_length=50, blank=True, null=True)  # Field name made lowercase.
    password = models.CharField(db_column='Password', max_length=30, blank=True, null=True)  # Field name made lowercase.
    personid = models.IntegerField(db_column='PersonID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'clients'


class Donations(models.Model):
    donationid = models.IntegerField(db_column='DonationID', primary_key=True)  # Field name made lowercase.
    amount = models.FloatField(db_column='Amount', blank=True, null=True)  # Field name made lowercase.
    method = models.CharField(db_column='Method', max_length=20, blank=True, null=True)  # Field name made lowercase.
    donationdate = models.DateField(db_column='DonationDate', blank=True, null=True)  # Field name made lowercase.
    taxreceipt = models.CharField(db_column='TaxReceipt', max_length=40, blank=True, null=True)  # Field name made lowercase.
    donorid = models.IntegerField(db_column='DonorID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'donations'


class Donors(models.Model):
    donorid = models.IntegerField(db_column='DonorID', primary_key=True)  # Field name made lowercase.
    personid = models.IntegerField(db_column='PersonID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'donors'


class Payments(models.Model):
    amountreceived = models.FloatField(db_column='AmountReceived', primary_key=True)  # Field name made lowercase.
    datepaid = models.DateField(db_column='DatePaid')  # Field name made lowercase.
    method = models.CharField(db_column='Method', max_length=20, blank=True, null=True)  # Field name made lowercase.
    clientid = models.IntegerField(db_column='ClientID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'payments'
        unique_together = (('amountreceived', 'datepaid', 'clientid'),)


class People(models.Model):
    personid = models.IntegerField(db_column='PersonID', primary_key=True)  # Field name made lowercase.
    address = models.CharField(db_column='Address', max_length=100, blank=True, null=True)  # Field name made lowercase.
    firstname = models.CharField(db_column='FirstName', max_length=20, blank=True, null=True)  # Field name made lowercase.
    lastname = models.CharField(db_column='LastName', max_length=20, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='Email', max_length=60, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'people'


class Requestdetails(models.Model):
    requestid = models.IntegerField(db_column='RequestID', primary_key=True)  # Field name made lowercase.
    requestcost = models.IntegerField(db_column='RequestCost', blank=True, null=True)  # Field name made lowercase.
    dateshipped = models.DateField(db_column='DateShipped', blank=True, null=True)  # Field name made lowercase.
    pullsheet = models.CharField(db_column='PullSheet', max_length=200, blank=True, null=True)  # Field name made lowercase.
    boxesrequested = models.IntegerField(db_column='BoxesRequested', blank=True, null=True)  # Field name made lowercase.
    datebookspulled = models.DateField(db_column='DateBooksPulled', blank=True, null=True)  # Field name made lowercase.
    datearrived = models.DateField(db_column='DateArrived', blank=True, null=True)  # Field name made lowercase.
    shippingmethod = models.CharField(db_column='ShippingMethod', max_length=20, blank=True, null=True)  # Field name made lowercase.
    shippingcost = models.FloatField(db_column='ShippingCost', blank=True, null=True)  # Field name made lowercase.
    requestcomplete = models.IntegerField(db_column='RequestComplete', blank=True, null=True)  # Field name made lowercase.
    surveycomplete = models.IntegerField(db_column='SurveyComplete', blank=True, null=True)  # Field name made lowercase.
    amountowed = models.FloatField(db_column='AmountOwed', blank=True, null=True)  # Field name made lowercase.
    paymentcomplete = models.IntegerField(db_column='PaymentComplete', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'requestdetails'


class Requests(models.Model):
    requestid = models.IntegerField(db_column='RequestID', primary_key=True)  # Field name made lowercase.
    datemade = models.DateField(db_column='DateMade', blank=True, null=True)  # Field name made lowercase.
    totalbooksrequested = models.IntegerField(db_column='TotalBooksRequested', blank=True, null=True)  # Field name made lowercase.
    clientid = models.IntegerField(db_column='ClientID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'requests'


class Staff(models.Model):
    staffid = models.IntegerField(db_column='StaffID', primary_key=True)  # Field name made lowercase.
    role = models.CharField(db_column='Role', max_length=50, blank=True, null=True)  # Field name made lowercase.
    personid = models.IntegerField(db_column='PersonID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'staff'


class Surveys(models.Model):
    surveyid = models.IntegerField(db_column='SurveyID', primary_key=True)  # Field name made lowercase.
    homeschooled = models.CharField(db_column='HomeSchooled', max_length=5, blank=True, null=True)  # Field name made lowercase.
    surveydate = models.DateField(db_column='SurveyDate', blank=True, null=True)  # Field name made lowercase.
    howdidyouhearaboutus = models.CharField(db_column='HowDidYouHearAboutUs', max_length=100, blank=True, null=True)  # Field name made lowercase.
    clientid = models.IntegerField(db_column='ClientID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'surveys'


class Works(models.Model):
    isbn = models.CharField(db_column='ISBN', primary_key=True, max_length=13)  # Field name made lowercase.
    genre = models.CharField(db_column='Genre', max_length=40, blank=True, null=True)  # Field name made lowercase.
    author = models.CharField(db_column='Author', max_length=40, blank=True, null=True)  # Field name made lowercase.
    title = models.CharField(db_column='Title', max_length=50, blank=True, null=True)  # Field name made lowercase.
    yearpublished = models.IntegerField(db_column='YearPublished', blank=True, null=True)  # Field name made lowercase.
    ltid = models.IntegerField(db_column='LTID', blank=True, null=True)  # Field name made lowercase.
    amazonid = models.IntegerField(db_column='AmazonID', blank=True, null=True)  # Field name made lowercase.
    googleid = models.IntegerField(db_column='GoogleID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'works'