from django.shortcuts import render
from django.urls import path
from django.conf.urls import url

from . import views

app_name = 'bookends'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
    url(r'^home/', views.HomeViewFunction),
    url(r'^payments/', views.PaymentsViewFunction),
    url(r'^login/', views.LoginViewFunction),
    url(r'^paymentstest/', views.PaymentsTestViewFunction),
    url(r'^books/', views.BooksViewFunction),
    url(r'^works/', views.WorksViewFunction),
    #path(r'^books/', views.BooksView.as_view(), name='books'),

]