
// Code for deleting tables
DROP TABLE IF EXISTS Payments CASCADE;
DROP TABLE IF EXISTS Clients CASCADE;
DROP TABLE IF EXISTS People CASCADE;
DROP TABLE IF EXISTS Staff CASCADE;
DROP TABLE IF EXISTS Donors CASCADE;
DROP TABLE IF EXISTS Children CASCADE;
DROP TABLE IF EXISTS Books CASCADE;
DROP TABLE IF EXISTS Works CASCADE;
DROP TABLE IF EXISTS Requests CASCADE;
DROP TABLE IF EXISTS RequestDetails CASCADE;
DROP TABLE IF EXISTS Surveys CASCADE;
DROP TABLE IF EXISTS Donations CASCADE;
DROP TABLE IF EXISTS Addresses CASCADE;

// Code for creating tables
CREATE TABLE Payments(
    AmountReceived FLOAT PRIMARY KEY,
    DatePaid DATE PRIMARY KEY,
    Method VARCHAR(20),
    ClientID INT,
    
);
CREATE TABLE Clients(
    ClientID INT PRIMARY KEY,
    RequestID INT,
    Username VARCHAR(50),
    Password VARCHAR(30),
    PersonID INT,
    FOREIGN KEY (RequestID) REFERENCES Requests(RequestID)
);

CREATE TABLE Books(
    BookID INT PRIMARY KEY,
    Available BOOLEAN DEFAULT NULL,
    Location VARCHAR(40),
    ISBN VARCHAR(13),
    RequestID INT,
    DonationID INT
    
);
CREATE TABLE Donors(
    DonorID INT PRIMARY KEY,
    PersonID INT
);
CREATE TABLE Staff(
    StaffID INT PRIMARY KEY,
    Role VARCHAR(50),
    PersonID INT
);
CREATE TABLE Children(
    ChildID INT PRIMARY KEY,
    ReadingLevel VARCHAR(40),
    Interests VARCHAR(50),
    ClientID INT,
    Name VARCHAR(50), 
    Age INT,
    SurveyID INT
);
CREATE TABLE People(
    PersonID INT PRIMARY KEY,
    Address VARCHAR(100),
    FirstName VARCHAR(20),
    LastName VARCHAR(20),
    Email VARCHAR(60)
  
);
CREATE TABLE Works(
    ISBN VARCHAR(13) PRIMARY KEY,
    Genre VARCHAR(40),
    Author VARCHAR(40),
    Title VARCHAR(50),
    YearPublished INT,
    LTID INT,
    AmazonID INT,
    GoogleID INT
);
CREATE TABLE Addresses(
    ShippingAddress VARCHAR(50) PRIMARY KEY,
    StateProvince VARCHAR(40),
    Country VARCHAR(60),
    City VARCHAR(30),
    SurveyID INT
);

CREATE TABLE Donations(
    DonationID INT PRIMARY KEY,
    Amount FLOAT,
    Method VARCHAR(20),
    DonationDate DATE,
    TaxReceipt VARCHAR(40),
    DonorID INT
);

CREATE TABLE Surveys(
    SurveyID INT PRIMARY KEY,
    HomeSchooled VARCHAR(5),
    SurveyDate DATE,
    HowDidYouHearAboutUs VARCHAR(100),
    ClientID INT
);
CREATE TABLE Requests(
    RequestID INT PRIMARY KEY,
    DateMade DATE,
    TotalBooksRequested INT,
    ClientID INT
);
CREATE TABLE RequestDetails(
    RequestID INT PRIMARY KEY,
    RequestCost INT,
    DateShipped DATE,
    PullSheet VARCHAR(200),
    BoxesRequested INT,
    DateBooksPulled DATE,
    DateArrived DATE DEFAULT NULL,
    ShippingMethod VARCHAR(20),
    ShippingCost FLOAT,
    RequestComplete BOOLEAN,
    SurveyComplete BOOLEAN,
    AmountOwed FLOAT,
    PaymentComplete BOOLEAN
);
