'''
Created by Kimberlee Gong, Chena Underhill 
4 October 2019
Last modified 6 Dec 2019 by Kimberlee Gong with real isbns
For populating a database with fake data
'''

from faker import Faker
import random
#code shell from https://www.w3schools.com/python/python_mysql_insert.asp
import mysql.connector
from datetime import datetime, timedelta



mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="passwordhere",
  database="nameofdatabasehere"
)


fake = Faker()
mycursor = mydb.cursor()


#Insert data into the people tableI
sqlPeople=  "INSERT INTO People (PersonID, Address, FirstName, LastName, Email) VALUES (%s, %s, %s, %s, %s)"
peopleList = []
for i in range (100): 
    line=(i, fake.street_address(), fake.first_name(), fake.last_name(), fake.email("west.edu"))
    peopleList.append(line)

mycursor.executemany(sqlPeople, peopleList)
print("inserted people")


#insert data into the workers table
sqlStaff=  "INSERT INTO Staff (StaffID,Role, PersonID) VALUES (%s, %s, %s)"
staffList = []
staffRoles= ["administrator", "volunteer", "librarian", "box packer"]

#10 staff
for i in range (10):
    line=(i, fake.word(ext_word_list=staffRoles), i)
    staffList.append(line)

mycursor.executemany(sqlStaff, staffList)
mydb.commit()
print("inserted staff")

#Insert data into donors table
sqlDonors=  "INSERT INTO Donors (DonorID, PersonID) VALUES (%s, %s)"
donorsList = []

#40 donors
for i in range (40):
    line=(i, i+10)
    donorsList.append(line)

mycursor.executemany(sqlDonors, donorsList)
mydb.commit()
print("inserted donors")




# Insert data into children table

readinglevel=["preK","1st","2nd","3rd","4th","5th","6th","junior high","high school"]
interests=["science","dinosaurs","knitting","the great outdoors", "adventure", "naps", "reading", "friends", "art", "crafts", "coding", "music", "violin", "other", "bothering siblings", "plants","aethetics","trees","databases","coloring","cooking","crafting"]
sqlChildren=  "INSERT INTO Children (ChildID, ReadingLevel, Interests, ClientID, Name, Age, SurveyID) VALUES (%s, %s, %s, %s, %s, %s, %s)"
childrenList = []

for i in range (25):
    line=(i,fake.word(ext_word_list=readinglevel), fake.word(ext_word_list=interests), i, fake.first_name(), random.randint(1,17), i)
    childrenList.append(line)

mycursor.executemany(sqlChildren, childrenList)
mydb.commit()
print("inserted children")




#Insert data into payment table
method=["Cash","Check","Card","Venmo"];
sqlPayment = "INSERT INTO payments (AmountReceived, DatePaid, Method, ClientID) VALUES (%s, %s, %s, %s)"
paymentList = []

for i in range (100):
    line = (random.randint(0, 100), fake.date(), fake.word(ext_word_list=method), random.randint(0, 100000))
    paymentList.append(line)
    
mycursor.executemany(sqlPayment, paymentList)
mydb.commit()
print("inserted payments")



# Insert data into donations table
sqlDonations=  "INSERT INTO Donations (DonationID, Amount, Method, DonationDate, TaxReceipt, DonorID) VALUES (%s, %s, %s, %s, %s, %s)"
donationsList = []

for i in range (1000):
    line=(i,random.randint(100, 10000), fake.word(ext_word_list=method), fake.date(), fake.ean(length=8), i)
    donationsList.append(line)

mycursor.executemany(sqlDonations, donationsList)
mydb.commit()
print("inserted donations")


# Insert data into surveys table
hearabout=["online","friend","God","friend of a friend","family","relative","organization","internet","yellow pages","aquaintance","tv advertisement"]
sqlSurveys=  "INSERT INTO Surveys (SurveyID, Homeschooled, SurveyDate, HowDidYouHearAboutUs, ClientID) VALUES (%s, %s, %s,%s, %s)"
surveysList = []

for i in range (1000):
    line=(i,random.randint(0,1), fake.date(), fake.word(ext_word_list=hearabout), i)
    surveysList.append(line)

mycursor.executemany(sqlSurveys, surveysList)
mydb.commit()
print("inserted surveys")


# Insert data into addresses table
sqlAddresses=  "INSERT INTO Addresses (ShippingAddress, StateProvince, Country, City, SurveyID) VALUES (%s, %s, %s, %s,%s)"
addressesList = []

for i in range (50):
    line=(fake.street_address(), fake.state(), fake.country(), fake.city(), i)
    addressesList.append(line)

mycursor.executemany(sqlAddresses, addressesList)
mydb.commit()
print("inserted addresses")



# Insert data into requests table
sqlRequests=  "INSERT INTO Requests (RequestID, DateMade, TotalBooksRequested, ClientID) VALUES (%s, %s, %s, %s)"
requestsList = []

for i in range (30):
    line=(i,fake.date(), random.randint(1,50), random.randint(0,50))
    requestsList.append(line)

mycursor.executemany(sqlRequests, requestsList)
mydb.commit()
print("inserted requests")


# Insert data into requestDetails table

shipping=["USPS","UPS","Friend","Pigeon","FedEx"]
sqlRequestDetails=  "INSERT INTO RequestDetails (RequestID, DateShipped,PullSheet, BoxesRequested, DateBooksPulled, DateArrived, ShippingMethod, ShippingCost, RequestComplete, SurveyComplete, AmountOwed, PaymentComplete) VALUES (%s, %s, %s, %s,%s, %s,%s, %s, %s, %s, %s, %s, %s)"
requestDetailsList = []

for i in range (30):
    dateShipped = fake.date()
    line=(i, dateShipped, fake.url(), random.randint(1,5), 
          datetime.strptime(dateShipped, '%Y-%m-%d') - timedelta(days=30), datetime.strptime(dateShipped, '%Y-%m-%d') + timedelta(days=91),
          fake.word(ext_word_list=shipping), 
          round(random.uniform(20.00,100.00), 2), random.randint(0,1), random.randint(0,1), 
          round(random.uniform(0.00,70.00), 2), random.randint(0,1))
    requestDetailsList.append(line)

mycursor.executemany(sqlRequestDetails, requestDetailsList)
mydb.commit()
print("inserted requestdetails")



# Insert data into clients table
sqlClients=  "INSERT INTO Clients (ClientID, Username, Password, PersonID) VALUES (%s, %s, %s, %s)"
clientsList = []

#50 clients
for i in range (50):
    line=(i,fake.word(), fake.word(), i+50)
    clientsList.append(line)

mycursor.executemany(sqlClients, clientsList)
mydb.commit()
print("inserted clients")



# Insert data into books table
location=["top shelf 1 left","top shelf 1 right","top shelf 1 middle","botton shelf 1 left","bottom shelf 1 right","middle shelf 2 right","middle shelf 3 left","bottom shelf 3 right","top shelf 3 middle","top shelf 4 middle","bottom shelf 5 middle","top shelf 5 left"]
sqlBooks=  "INSERT INTO Books (BookID, Available, Location, ISBN, RequestID, DonationID) VALUES (%s, %s, %s, %s, %s, %s)"
isbn =[ 9780060254933,  9780060256654,  9780064430173, 9780394800165,  9780689306471,  9780399208539, 9780689300721,  9781564024732,  9780307120007, 9780920668368,  9780394823379,  9780811826020, 9780671449018,  9780805017441,   9780064430227,   9780671449025,  9780811879545,  9780060776398, 9780786819881 ,  9780810918689,  9781596434028,  9780590930024, 9780916291457,  9780394900209, 9780439339117,  9780671679491, 9780399224454,  9780060542092 , 9780140501698,  9780316013567 , 9780618194575 ,  9780140501735, 9780152802172,   9780394800189 , 9780679805274,   9780689829536 , 9780671662837,   9780061123221,   9780394900131 ,    9780395150238,  9780395304488,  9780689504761, 9780689832130 ,  9780689835605,  9780399244674,   9780723234609,  9780394818238,   9780060245863, 9780878685851,   9780395169612, 9780201091472,    9780312368401,  9780395411056,   9780395551134,  9780805006629,  9780810908512 , 9780763655983,  9780060266684,  9780670827596,  9780060207663, 9781416958727,  9780618428588,  9780812055726 ,  9780618756636 ,  9780618256280,  9781558580091, 9780670867332 ,  9780670445806,   9780395181560 ,    9780920236161,  9780761147633 ,   9780152567088,  9780590316811 ,  9780394800028, 9780670451494 ,  9780679809012 , 9780395389492 ]   
booksList = []

for i in range (74):
    line=(i,random.randint(0,1), fake.word(ext_word_list=location), isbn[i], i+4, random.randint(1,1000))
    booksList.append(line)

mycursor.executemany(sqlBooks, booksList)
mydb.commit()
print("inserted books")

genres=["Action","Adventure","Childrens","Crime", "Drama","Fairytale","Fantasy","Historical Fiction","Mystery","Picture","Poetry","Romance","Science Fiction","Thriller"]

# Insert data into works table
sqlWorks=  "INSERT INTO Works (ISBN, Genre, Author, Title, YearPublished, LTID, AmazonID, GoogleID) VALUES (%s, %s, %s, %s,%s, %s, %s, %s)"
worksList = []

for i in range (70):
    line=(isbn[i],fake.word(ext_word_list=genres), fake.name(), fake.word(), fake.year(), random.randint(1,20000), random.randint(1,20000), random.randint(1,20000))
    worksList.append(line)

mycursor.executemany(sqlWorks, worksList)
mydb.commit()
print("inserted works")