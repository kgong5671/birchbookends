READ ME for Team Birch Phase 5:

Group Members: Kimberly Gong, Chena Underhill, Blake Dayman, Sam Amundson

Help Recieved:
Codingshiksha on youtube for Google Books API
Django Project Tutorial: Writing Your First Django App
Django Documentation
Bootstrap Documentation
W3schools.com

Instructions for running the project:
First create a database named cs125 and use initialize.sql to create tables.
Next, run the fakeData.py script to populate the database.
Running this python script so that it populates the database requires open the script and enterng your own credentials on lines 16-21.
Now to run the django project.
In the file mysite/mysite/settings.py, sonfigure databases to hold login information for the previously created database.
Run the server with -python manage.py runserver
Go to the localhost:8000/bookends/home/
After this, you should be good to go!

Who worked on what:
Kimberlee Gong was instrumental in the Django programming.
Chena Underhill owned the bootstrap stuff.
Blake Dayman helped pair program the Django stuff and worked on building a search bar.
Sam Amundson learned about Python and Django.

BITBUCKET URL: https://bitbucket.org/kgong5671/birchbookends/src


